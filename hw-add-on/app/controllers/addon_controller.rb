class AddonController < ApplicationController
  def greeting
  end

  def descriptor
    respond_to do |format|
      format.json
    end
  end
end